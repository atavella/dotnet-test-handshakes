using System;
using Xunit;

namespace Pipe.Tests.Handshakes
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.True(new Handshakes(0).Calculate() == 0);
        }

        [Fact]
        public void Test2()
        {
            Assert.True(new Handshakes(1).Calculate() == 0);
        }

        [Fact]
        public void Test3()
        {
            Assert.True(new Handshakes(2).Calculate() == 1);
        }

        [Fact]
        public void Test4()
        {
            Assert.True(new Handshakes(3).Calculate() == 3);
        }

        [Fact]
        public void Test5()
        {
            Assert.True(new Handshakes(4).Calculate() == 6);
        }

        [Fact]
        public void Test6()
        {
            Assert.True(new Handshakes(5).Calculate() == 10);
        }

        [Fact]
        public void Test7()
        {
            Assert.True(new Handshakes(-1).Calculate() == 0);
        }

    }
}
